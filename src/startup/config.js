
require('dotenv').config();
const convict = require('convict');
const schema = require('./config/environment');

const config = convict(schema);
const env = config.get('env');
config.loadFile(`./src/startup/environment/${env}.json`);
config.validate({ strict: true });

module.exports.config = config;
module.exports.port = config.get('port');
module.exports.env = env;
