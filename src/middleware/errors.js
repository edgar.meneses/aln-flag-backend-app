// eslint-disable-next-line no-unused-vars
module.exports.errorMiddleware = (error, req, res, next) => {
  res.status(500)
    .json({ message: 'Error' });
};

module.exports.notFoundMiddleware = (req, res) => {
  res.status(404)
    .json({ message: 'Not Found' });
};
