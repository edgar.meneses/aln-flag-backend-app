FROM node:10
WORKDIR /home/node/app
COPY package.json .
COPY .npmrc .
RUN npm install --production
ENV NODE_ENV #{env}#
COPY . .
USER node
EXPOSE 9680
CMD ["node", "app.js"]
