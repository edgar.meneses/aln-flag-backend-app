const express = require('express');
const winston = require('winston');

const app = express();
const { port, env } = require('./src/startup/config');
require('./src/startup/routes')(app);

const server = app.listen(port, () => {
  winston.log('info', `Server running at port ${port} and environment ${env}`);
});

module.exports = server;
